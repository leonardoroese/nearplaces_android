Under LGPL Licence v3.0
[LGPL](https://www.gnu.org/licenses/lgpl-3.0.en.html)

This application provides a simple list of specific places near by user's location.

### Android Native Development using Google Maps API ###

Currently composed my an introduction activity for a specific place (ex.: Gas Stations) on a fixed radius. It calls a "list" Activity containing the list of places ordered by proximity displaying the place name, address, distance, description and an action button to open on Google Maps Navigation giving directions to reach current target.

### Applications ###

You can add to your existing project and customize it to display near places as shortcut or with an Intro. Screen displaying information about the application such:

- Out of gas? Check near Gas Stations.
- Need parking? Checkout around you now!
- Tired, how abou a coffee? Checkout available Starbucks near by!

You can customize for your business ;-)

### Requirements ###

- Android API 15+ (4.0.3 - 7.0target)
- Google Places API Key (https://developers.google.com/places/android-api/signup?hl=pt-br)
- LRWS wsCaller modified class (https://github.com/leonardoroese/LRWS)


### SCREEN CAPTURE ###


![np1.png](https://bitbucket.org/repo/opKygR/images/2995125847-np1.png)
![np2.png](https://bitbucket.org/repo/opKygR/images/1990188412-np2.png)