package br.com.zpi.nearplaces;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends MyActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imgGas = (ImageView) findViewById(R.id.imgGas);

        checkInternetConnection();
        requestLocation();

        imgGas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, actPlaceList.class);
                startActivity(i);
                finish();
            }
        });

    }
}
