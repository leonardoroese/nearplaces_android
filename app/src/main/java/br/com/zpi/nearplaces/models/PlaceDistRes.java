package br.com.zpi.nearplaces.models;

/**
 * Created by leonardo on 28/11/16.
 */

public class PlaceDistRes {
    public String[] destination_addresses = null;
    public String[] origin_addresses = null;
    public PlaceDistElem[] rows = null;
    public String status = null;
}
