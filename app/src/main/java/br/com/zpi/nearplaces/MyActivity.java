package br.com.zpi.nearplaces;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.view.WindowManager;

public class MyActivity extends Activity implements LocationListener {


    private AlertDialog alertD = null;
    private boolean isConnected = false;
    private static final int PERM_LOCATION = 101;
    public Double last_lat = null;
    public Double last_lon = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.np_greenbg));
            window.setNavigationBarColor(getResources().getColor(R.color.np_greenbg));
        }
    }

//##############################################################################################
    // PERMISSION RESULT
    //##############################################################################################

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERM_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    updateLocation();
                }
            }

        }
    }


    //##############################################################################################
    // LOCATION CHANGE
    //##############################################################################################
    @Override
    public void onLocationChanged(Location location) {
        refreshMyLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        refreshMyLocation();
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }



    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // UPDATE LOCATION
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    public void requestLocation() {

        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                updateLocation();
                return;
            } else {

                ActivityCompat.requestPermissions(MyActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERM_LOCATION);
            }
        }else{
            updateLocation();
        }

    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // UPDATE LOCATION
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    public void updateLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                refreshMyLocation();
                if(last_lon == null) {
                    alertD = new AlertDialog.Builder(this).create();
                    alertD.setTitle(getResources().getText(R.string.alertdiag_error).toString());
                    alertD.setMessage(getResources().getText(R.string.generic_msg_e_nolocation).toString());
                    alertD.show();
                }
            }
        }else{
            LocationManager myLocation = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                myLocation.requestSingleUpdate(LocationManager.GPS_PROVIDER, this, null);
            else if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED)
                myLocation.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, this, null);
            refreshMyLocation();
        }
    }

    //----------------------------------------------------------------------------------------------
    // REFRESH LOCATION
    //----------------------------------------------------------------------------------------------
    public void refreshMyLocation() {
        LocationManager myLocation = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            last_lat = null;
            last_lon = null;
            Location loc = myLocation.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (loc == null)
                loc = myLocation.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (loc == null)
                loc = myLocation.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (loc != null) {
                last_lat = loc.getLatitude();
                last_lon = loc.getLongitude();
            }
        } catch (SecurityException ex) {

        }
    }

    //----------------------------------------------------------------------------------------------
    // CHECK INTERNET CONNECTION
    //----------------------------------------------------------------------------------------------
    public boolean checkInternetConnection() {

        //Check Network state
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (conMgr.getActiveNetworkInfo() != null && (conMgr.getActiveNetworkInfo().getState() == NetworkInfo.State.CONNECTED || conMgr.getActiveNetworkInfo().getState() == NetworkInfo.State.CONNECTING)) {
            isConnected = true;
            return true;
        }
        alertD = new AlertDialog.Builder(this).create();
        alertD.setTitle(getResources().getText(R.string.alertdiag_error).toString());
        alertD.setMessage(getResources().getText(R.string.generic_msg_e_nointernet).toString());
        alertD.show();
        isConnected = false;
        return false;
    }
}
