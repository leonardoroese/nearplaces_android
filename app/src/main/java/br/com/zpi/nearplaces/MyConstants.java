package br.com.zpi.nearplaces;

/**
 * Created by leonardo on 28/11/16.
 */

public class MyConstants {
    public static final String GOOGLE_DISTANCEMATRIX_API_KEY = "AIzaSyCTlVfW3-Aulif-4SRbjlu1e8UZHhikPeE";
    public static final String GOOGLE_PLACES_API_KEY = "AIzaSyCTlVfW3-Aulif-4SRbjlu1e8UZHhikPeE";

    public static final String GPLACESAPI_URL = "https://maps.googleapis.com/maps/api/place/";
    public static final String GDISTMATRIXAPI_URL = "https://maps.googleapis.com/maps/api/distancematrix/";
    public static final String GDISTMATRIXAPI_ENDPOINT = "json";
    public static final String GPLACESAPI_ENDPOINT = "nearbysearch/json";

    public static final String myRadius = "5000";

    public static final int resultLIMIT = 10;
}
