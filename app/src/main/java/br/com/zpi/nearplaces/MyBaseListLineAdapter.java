package br.com.zpi.nearplaces;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

public class MyBaseListLineAdapter<T> extends BaseAdapter {
    private ArrayList<T> content;

    public MyBaseListLineAdapter(Context context, ArrayList<T> content) {
        super();
        this.content = content;
    }

    @Override
    public int getCount() {
        return content.size();
    }

    @Override
    public Object getItem(int i) {
        return content.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = (View) getContent().get(i);
        return v;
    }

    public ArrayList<T> getContent() {
        return this.content;
    }

    public void setContent(ArrayList<T> content) {
        this.content = content;
    }
}
