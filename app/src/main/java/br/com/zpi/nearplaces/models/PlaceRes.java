package br.com.zpi.nearplaces.models;

/**
 * Created by leonardo on 27/11/16.
 */

public class PlaceRes {
    public String html_attributions = null;
    public String next_page_token = null;
    public Place[] results = null;
    public String status = null;
}
