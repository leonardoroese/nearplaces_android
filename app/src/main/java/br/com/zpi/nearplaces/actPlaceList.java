package br.com.zpi.nearplaces;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.zpi.nearplaces.models.Place;
import br.com.zpi.nearplaces.models.PlaceDistElemItem;
import br.com.zpi.nearplaces.models.PlaceDistRes;
import br.com.zpi.nearplaces.models.PlaceRes;
import br.com.zpi.nearplaces.ws.wsCall;

public class actPlaceList extends MyActivity {

    private static int resPage = 0;
    private static int curPage = 0;
    private boolean resEnd = false;
    private MyTask task = null;
    private ListView lstDet = null;
    private RelativeLayout layLoading = null;

    //##############################################################################################
    // CREATE
    //##############################################################################################

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_place_list);
        lstDet = (ListView) findViewById(R.id.lstDet);
        layLoading = (RelativeLayout) findViewById(R.id.layLoading);

        ImageView imgRefresh = (ImageView) findViewById(R.id.imgRefresh);

        imgRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listPlaces(true);
            }
        });



        listPlaces(true);

    }


    //----------------------------------------------------------------------------------------------
    // LOAD PLACES
    //----------------------------------------------------------------------------------------------

    public void listPlaces(boolean resetpaging) {

        checkInternetConnection();
        requestLocation();

        if (task != null && task.getStatus() == AsyncTask.Status.RUNNING) {
            if (!resetpaging)
                return;
            else
                task.cancel(true);
        }

        if (resetpaging) {
            resPage = 0;
            lstDet.setAdapter(null);
            lstDet.bringToFront();
            resEnd = false;
        }
        if (resEnd) {
            return;
        }

        task = new MyTask() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                setLoading(true);
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                PlaceRes placeres = null;
                wsCall wsC = new wsCall(getBaseContext());
                String[][] pars = new String[6][2];
                pars[0][0] = "location";
                pars[0][1] = String.valueOf(last_lat) + "," + String.valueOf(last_lon);
                pars[1][0] = "types";
                pars[1][1] = "gas_station";
                pars[2][0] = "key";
                pars[2][1] = MyConstants.GOOGLE_PLACES_API_KEY;
                pars[3][0] = "rankby";
                pars[3][1] = "distance";
                pars[4][0] = "sensor";
                pars[4][1] = "false";
                pars[5][0] = "language";
                pars[5][1] = getResources().getText(R.string.gapilang).toString();
                // CALL PLACES NEARBY
                try {
                    placeres = (PlaceRes) wsC.call(PlaceRes.class.getName(), MyConstants.GPLACESAPI_URL, MyConstants.GPLACESAPI_ENDPOINT, pars, wsCall.METHOD_GET);
                    if (placeres != null && placeres.results != null && placeres.results.length > 0) {
                        String distTo = "";
                        String distFrom = "";
                        int c = 0;
                        for (Place p : placeres.results) {
                            if(MyConstants.resultLIMIT > 0 && c >= MyConstants.resultLIMIT)
                                break;
                            if (distTo.trim().length() > 0)
                                distTo = distTo + "|";
                            distTo = distTo + p.geometry.location.lat + "," + p.geometry.location.lng;
                            if (distFrom.trim().length() > 0)
                                distFrom = distFrom + "|";
                            distFrom = distFrom + String.valueOf(last_lat) + "," + String.valueOf(last_lon);
                            c++;
                        }
                        pars = new String[6][2];
                        pars[0][0] = "origins";
                        pars[0][1] = distFrom;
                        pars[1][0] = "destinations";
                        pars[1][1] = distTo;
                        pars[2][0] = "key";
                        pars[2][1] = MyConstants.GOOGLE_DISTANCEMATRIX_API_KEY;
                        pars[3][0] = "mode";
                        pars[3][1] = "driving";
                        pars[4][0] = "language";
                        pars[4][1] = getResources().getText(R.string.gapilang).toString();
                        pars[5][0] = "units";
                        pars[5][1] = "metric";
                        // CALL DISTANCE MATRIX
                        try {
                            wsC.setUrlencoded(false);
                            PlaceDistRes distM = (PlaceDistRes) wsC.call(PlaceDistRes.class.getName(), MyConstants.GDISTMATRIXAPI_URL, MyConstants.GDISTMATRIXAPI_ENDPOINT, pars, wsCall.METHOD_GET);
                            if (distM != null && distM.rows != null && distM.rows.length > 0 && distM.rows[0].elements.length > 0) {
                                int cnt = 0;
                                for (PlaceDistElemItem i : distM.rows[0].elements) {
                                    if (i.distance != null) {
                                        placeres.results[cnt].distance = i.distance.text;
                                    }
                                    cnt++;
                                }
                            }
                        } catch (wsCall.LRWSException ex) {

                        }
                    }
                    return placeres;
                } catch (wsCall.LRWSException e) {
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                setLoading(false);
                TextView txtNoResult = (TextView) findViewById(R.id.txtNoResult);
                txtNoResult.setVisibility(View.GONE);

                if (o != null) {
                    PlaceRes placesres = (PlaceRes) o;
                    if (placesres.results != null && placesres.results.length > 0) {
                        ArrayList<Place> a = new ArrayList<Place>();
                        if (resPage == 0) {
                            int cnt = 0;
                            for (Place c : placesres.results) {
                                if(MyConstants.resultLIMIT > 0 && cnt >= MyConstants.resultLIMIT)
                                    break;
                                if (c != null)
                                    a.add(c);
                                cnt++;
                            }
                            MyBaseListLineAdapter ad = new MyBaseListLineAdapter(getBaseContext(), a) {
                                @Override
                                public View getView(int i, View view, ViewGroup viewGroup) {
                                    Place p = (Place) getContent().get(i);
                                    String lat = null, lng = null;

                                    if (!resEnd && i > 0 && i >= (getContent().size() - 3)) {
                                        resPage = i;
                                        listPlaces(false);
                                    }
                                    if (p.geometry != null && p.geometry.location != null) {
                                        lat = p.geometry.location.lat;
                                        lng = p.geometry.location.lng;
                                    }

                                    return drawPlace(p.name, p.vicinity, p.rating, p.price_level, lat, lng, p.distance);
                                }
                            };
                            lstDet.setAdapter(ad);
                            resEnd = true; // just 1 page for now
                        } else {
                            MyBaseListLineAdapter ad = (MyBaseListLineAdapter) lstDet.getAdapter();
                            a = ad.getContent();
                            for (Place c : placesres.results)
                                a.add(c);
                            ad.setContent(a);
                            ad.notifyDataSetChanged();
                        }
                    } else {
                        resEnd = true;
                    }
                } else {
                    resEnd = true;
                    txtNoResult.setVisibility(View.VISIBLE);
                }
            }
        };

        task.execute();
    }


    //----------------------------------------------------------------------------------------------
    // DRAW LINE
    //----------------------------------------------------------------------------------------------

    public RelativeLayout drawPlace(String name, String vicinity, String rate, String price, String lat, String lng, String distance) {
        LayoutInflater inf = getLayoutInflater();
        RelativeLayout lay = (RelativeLayout) inf.inflate(R.layout.line_placedetail, null);

        TextView txtPlaceName = (TextView) lay.findViewById(R.id.txtPlaceName);
        TextView txtPlaceAddr = (TextView) lay.findViewById(R.id.txtPlaceAddr);
        TextView txtPlaceDist = (TextView) lay.findViewById(R.id.txtPlaceDist);

        RelativeLayout layEval = (RelativeLayout) lay.findViewById(R.id.layEval);
        FrameLayout layEvalRate = (FrameLayout) lay.findViewById(R.id.layEvalRate);
        ImageView imgEval = (ImageView) lay.findViewById(R.id.imgEval);

        RelativeLayout layPrice = (RelativeLayout) lay.findViewById(R.id.layPrice);
        FrameLayout layPriceRate = (FrameLayout) lay.findViewById(R.id.layPriceRate);
        ImageView imgPrice = (ImageView) lay.findViewById(R.id.imgPrice);

        Button btnGo = (Button) lay.findViewById(R.id.btnGo);

        if (name != null && name.trim().length() > 0)
            txtPlaceName.setText(name);
        else
            txtPlaceName.setText("--");

        if (vicinity != null && vicinity.trim().length() > 0)
            txtPlaceAddr.setText(vicinity);
        else
            txtPlaceAddr.setVisibility(View.GONE);

        // CALCULATE EVALUTATION RATE
        if (rate != null && rate.trim().length() > 0) {
            layEval.setVisibility(View.VISIBLE);
            Double evW = Double.valueOf(getResources().getDimensionPixelSize(R.dimen.line_detail_evalw));
            evW = evW * (Double.valueOf(rate) * 20 / 100);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(evW.intValue(), RelativeLayout.LayoutParams.MATCH_PARENT);
            layEvalRate.setLayoutParams(lp);
            imgEval.bringToFront();
        }else
            layEval.setVisibility(View.GONE);

        // CALCULATE PRICE RATE
        if (price != null && price.trim().length() > 0) {
            layPrice.setVisibility(View.VISIBLE);
            Double evW = Double.valueOf(getResources().getDimensionPixelSize(R.dimen.line_detail_evalw));
            evW = evW * (Double.valueOf(price) * 20 / 100);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(evW.intValue(), RelativeLayout.LayoutParams.MATCH_PARENT);
            layPriceRate.setLayoutParams(lp);
            imgPrice.bringToFront();
        }else
            layPrice.setVisibility(View.GONE);

        if (distance != null) {
            txtPlaceDist.setText(distance);
        } else {
            txtPlaceDist.setVisibility(View.GONE);
        }

        if (lat != null && lng != null) {
            btnGo.setTag(lat + "," + lng);
            btnGo.setVisibility(View.VISIBLE);
            btnGo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                Uri.parse("http://maps.google.com/maps?daddr=" + view.getTag().toString()));
                        startActivity(intent);
                    } catch (Exception e) {

                    }
                }
            });
        } else {
            btnGo.setVisibility(View.GONE);
        }

        return lay;
    }

    public void setLoading(boolean vis){
        if(vis)
            layLoading.setVisibility(View.VISIBLE);
        else
            layLoading.setVisibility(View.GONE);
    }


}
