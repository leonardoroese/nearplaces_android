package br.com.zpi.nearplaces.models;

/**
 * Created by leonardo on 28/11/16.
 */

public class PlaceDistElemItem {
    public PlaceDistTextValue distance = null;
    public PlaceDistTextValue duration = null;
    public String status = null;
}
