package br.com.zpi.nearplaces.ws;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

/**
 *  A part of project LRWS https://github.com/leonardoroese/LRWS under GNU Lesser Licence
 *  Author: Leonardo Germano Roese
 */
public class wsCall {

    public final static int METHOD_GET = 1;
    public final static int METHOD_POST = 2;
    public final static int METHOD_PUT = 3;
    public final static int METHOD_DELETE = 4;

    private String[] arrMeth = new String[]{"GET", "POST", "PUT", "DELETE"};
    private String auth_user = null;
    private String auth_pass = null;
    private String auth_string = null;
    private Context ctx = null;

    public String resType = null;
    public String resMsg = null;

    private boolean urlencoded = true;

    //----------------------- CONSTRUCTOR (Auth basic)
    public wsCall(Context ctx, String user, String pass) {
        auth_user = user;
        auth_pass = pass;
        this.ctx = ctx;
    }

    //----------------------- CONSTRUCTOR (Auth JWT)
    public wsCall(Context ctx, String authStr) {
        auth_string = authStr;
        this.ctx = ctx;

    }

    //----------------------- CONSTRUCTOR (Auth JWT)
    public wsCall(Context ctx) {
        this.ctx = ctx;

    }

    public void setUrlencoded(boolean urlencoded) {
        this.urlencoded = urlencoded;
    }

    //##############################################################################################
    // CALL ACTIONS
    //##############################################################################################
    private String callWSAS(String host, String name, String[][] params, int method) throws LRWSException {
        return callWSAS(host, name, params, method, null);
    }

    private String callWSAS(String host, String name, String[][] params, int method, Object jobj) throws LRWSException {
        String[] sendpar = null;
        if (host == null || host.trim().length() <= 0) {
            throw new LRWSException(LRWSException.E_PARAMS, "Forgot to inform host endpoint?");
        }
        if (name == null || name.trim().length() <= 0) {

            throw new LRWSException(LRWSException.E_PARAMS, "Forgot to inform endpoint name?");
        }

        if (method != METHOD_GET && method != METHOD_POST && method != METHOD_PUT && method != METHOD_DELETE) {
            throw new LRWSException(LRWSException.E_PARAMS, "Invalid Method");
        }

        switch (method) {
            case METHOD_GET:
                try {
                    if(jobj != null)
                        return doGET(host + name, params, false, jobj);
                    else
                        return doGET(host + name, params);
                } catch (LRWSException e) {
                    throw e;
                }
            case METHOD_POST:
                try {
                    return doPOST(host + name, params, false, jobj);
                } catch (LRWSException e) {
                    throw e;
                }
            case METHOD_PUT:
                try {
                    return doPUT(host + name, params, false, jobj);
                } catch (LRWSException e) {
                    throw e;
                }

            case METHOD_DELETE:
                try {
                    return doDELETE(host + name, params, false, jobj);
                } catch (LRWSException e) {
                    throw e;
                }
        }
        return null;
    }

    //##############################################################################################
    // GET
    //##############################################################################################
    private String doGET(String endpoint, String[][] params) throws LRWSException {
        return doGET(endpoint, params, false, null);
    }

    private String doGET(String endpoint, String[][] params, boolean jsonpars, Object jobj) throws LRWSException {
        return execHttpCall(endpoint,params,jsonpars,jobj,METHOD_GET);

    }

    //##############################################################################################
    // POST
    //##############################################################################################

    private String doPOST(String endpoint, String[][] params, boolean jsonpars, Object jobj) throws LRWSException {
        return execHttpCall(endpoint,params,jsonpars,jobj,METHOD_POST);
    }

    //##############################################################################################
    // PUT
    //##############################################################################################

    private String doPUT(String endpoint, String[][] params, boolean jsonpars, Object jobj) throws LRWSException {
        return execHttpCall(endpoint,params,jsonpars,jobj,METHOD_PUT);
    }

    //##############################################################################################
    // DELETE
    //##############################################################################################

    private String doDELETE(String endpoint, String[][] params, boolean jsonpars, Object jobj) throws LRWSException {
        return execHttpCall(endpoint,params,jsonpars,jobj,METHOD_DELETE);
    }

    //##############################################################################################
    // EXEC HTTP CALL
    //##############################################################################################
    private String execHttpCall(String endpoint, String[][] params, boolean jsonpars, Object jobj, int method) throws LRWSException {
        String authbasic = null;
        String authjwt = null;
        String parcon = "";
        String linha = "";
        JSONObject jsonP = new JSONObject();


        // Prepare Authentication String
        if (auth_user != null && auth_user.trim().length() > 0)
            authbasic = Base64.encodeToString(new String(auth_user.toLowerCase() + ":" + auth_pass).getBytes(),Base64.NO_PADDING);
        else if (auth_string != null && auth_string.trim().length() > 0)
            authjwt = auth_string;

        //Prepare output Parameters

        if (jsonpars) {
            //JSON MODE
            if (jobj != null) {
                jsonP = mod2json(jobj, false, true);
            } else if (params != null && params.length > 0) {
                for (int i = 0; i < params.length; i++) {
                    try {
                        jsonP.put(params[i][0], params[i][1]);
                    } catch (Exception e) {

                    }
                }
            }
        } else {
            //POST MODE
                if (params != null && params.length > 0) {
                    for (int i = 0; i < params.length; i++) {
                        if(params[i][0] != null)
                            try {
                                if(urlencoded)
                                    parcon = parcon + "&" + params[i][0] + "=" + URLEncoder.encode(params[i][1],"UTF-8");
                                else
                                    parcon = parcon + "&" + params[i][0] + "=" + params[i][1];
                            } catch (UnsupportedEncodingException e) {

                            }
                    }
                }
        }

        HttpURLConnection connection = null;

        try {

            //Concatenate parameters to endpoint
            if (parcon != null && parcon.length() > 0 && method == METHOD_GET)
                endpoint = endpoint + "?" + parcon.substring(1);

            URL url = new URL(endpoint);
            connection = (HttpURLConnection) url.openConnection();

            // Pre-set for post
            if(method == METHOD_POST || method == METHOD_PUT) {
                connection.setDoOutput(true);
            }else{
                connection.setDoOutput(false);
            }
            connection.setRequestMethod(arrMeth[method - 1]);
            connection.setInstanceFollowRedirects(false);
            if (authbasic != null && authbasic.trim().length() > 0)
                connection.setRequestProperty("Authorization", "Basic " + authbasic);
            if (authjwt != null && authjwt.trim().length() > 0)
                connection.setRequestProperty("JWT", authjwt);

            if (jsonpars && jobj != null) {

                connection.setRequestProperty("Content-Type", "application/json");
                if(method == METHOD_GET)
                    connection.setRequestProperty("Transfer-Encoding", "chunked");

                OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
                osw.write(jsonP.toString());
                osw.flush();
                osw.close();
            }else if(method != METHOD_GET && parcon != null && parcon.trim().length() > 0){
                connection.setDoOutput(true);
                byte[] postData  = parcon.getBytes();
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty( "charset", "utf-8");
                connection.setRequestProperty( "Content-Length", String.valueOf(postData.length));
                connection.setUseCaches( false );
                OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
                osw.write(parcon);
                osw.flush();
                osw.close();
            }

            int stt = connection.getResponseCode();
            try {
                BufferedInputStream is = new BufferedInputStream(connection.getInputStream());
                if (is != null) {
                    try {
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));
                        String inputLine = "";
                        while ((inputLine = br.readLine()) != null) {
                            linha = linha + inputLine;
                        }
                    } catch (Exception e) {
                        linha = "";
                    } finally {
                        if (is != null)
                            is.close();
                    }
                }
            }catch(Exception e){
                String emsg = e.getMessage();
            }
            if (stt >= 400d) {
                connection.disconnect();
                throw new LRWSException(String.valueOf(stt), linha);

            }else{
                connection.disconnect();

            }

            return linha;

        } catch (Exception e) {
            if(connection != null)
                connection.disconnect();

            throw new LRWSException(LRWSException.E_CALLERROR, e.getMessage());

        }finally{
            if(connection != null)
                connection.disconnect();
        }

    }


    //##############################################################################################
    // CALL WEBSERVICE EXPECTING OBJECT[] RETURN
    //##############################################################################################

    public Object[] callArray(String classname, String arrayparam, String wshost, String wsname, String[][] params, int method) throws LRWSException {
        return callArray(classname, arrayparam, wshost, wsname, params, method, null);
    }

    public Object[] callArray(String classname, String arrayparam, String wshost, String wsname, String[][] params, int method, Object jobj) throws LRWSException {

        if (classname == null || classname.trim().length() <= 0) {
            throw new LRWSException(LRWSException.E_WRAPPER, "You have to inform a classname.");
        }

        String callres = null;

        try {
            callres = callWSAS(wshost, wsname, params, method, jobj);
        } catch (LRWSException e) {
            throw e;
        }
        if (callres != null && callres.trim().indexOf("[") >= 0) {
            try {
                JSONArray jsa = null;
                if (arrayparam != null && arrayparam.trim().length() > 0) {
                    JSONObject jso = new JSONObject(callres);
                    if (jso.has(arrayparam)) jsa = jso.getJSONArray(arrayparam);
                } else {
                    jsa = new JSONArray(callres);
                }
                if (jsa != null) {
                    Object uout = Array.newInstance(Class.forName(classname), jsa.length());
                    Object[] rout = (Object[]) uout;
                    return json2ObjectA(rout, jsa, true);
                }
                return null;
            } catch (Exception ex) {
                throw new LRWSException(LRWSException.E_CONVERSION, "Error parsing Data");
            }
        } else {
            throw new LRWSException(LRWSException.E_CALLERROR, "Web Service not executed");
        }
    }

    //##############################################################################################
    // CALL WEBSERVICE EXPECTING OBJECT RETURN
    //##############################################################################################


    public Object call(String classname, String wshost, String wsname, String[][] params, int method) throws LRWSException {
        return call(classname, wshost, wsname, params, method, null);
    }

    public Object call(String classname, String wshost, String wsname, String[][] params, int method, Object jobj) throws LRWSException {
        if (classname == null || classname.trim().length() <= 0) {
            throw new LRWSException(LRWSException.E_WRAPPER, "You have to inform a classname.");
        }
        String callres = null;
        try {
            callres = callWSAS(wshost, wsname, params, method, jobj);
        } catch (LRWSException e) {
            throw e;
        }
        if (callres != null) {
            if(callres.trim().length() <= 0)
                return null;
            if(callres.trim().substring(0,2).equals("E:")){
                resType = "E";
                resMsg = callres.trim().substring(2);
                return null;
            }else if(callres.trim().substring(0,2).equals("S:")){
                resType = "S";
                callres = callres.trim().substring(2);
            }
            try {
                Object ref = Class.forName(classname).newInstance();
                JSONObject jso = new JSONObject(callres);
                return json2Object(ref, jso, true);
            } catch (Exception ex) {
                throw new LRWSException(LRWSException.E_CONVERSION, "Error parsing Data");
            }
        } else {
            throw new LRWSException(LRWSException.E_CALLERROR, "Web Service not executed");
        }
    }

    //##############################################################################################
    // CALL WEBSERVICE EXPECTING STRING RETURN
    //##############################################################################################

    public String callSimple(String wshost, String wsname, String[][] params, int method) throws LRWSException {
        return  callSimple(wshost,  wsname, params, method, null);
    }

    public String callSimple(String wshost, String wsname, String[][] params, int method, Object jobj) throws LRWSException {
        return callWSAS(wshost, wsname, params, method,jobj);
    }

    // ####################################################################
    // JSONObject to Java Line Object
    // ####################################################################

    public Object json2Object(Object o, JSONObject jso, boolean decode){
        if(jso == null)
            return null;
        if(o == null)
            return null;
        if(jso.length() <= 0)
            return null;

        JSONArray jsa = new JSONArray();
        Iterator<String> iter = jso.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                if(jso.get(key).getClass().equals(jsa.getClass())){
                    String clname = o.getClass().getDeclaredField(key).getType().getName();
                    if(clname.trim().indexOf("[") == 0)
                        clname = clname.substring(2, clname.length() - 1);
                    Object[] ao = (Object[]) Array.newInstance(Class.forName(clname),((JSONArray)jso.get(key)).length());
                    int cnob = 0;
                    for(Object xo : ao){
                        ao[cnob] = Class.forName(clname).newInstance();
                        cnob++;
                    }
                    ao = json2ObjectA(ao, (JSONArray) jso.get(key), decode);
                    if(ao != null){
                        o.getClass().getDeclaredField(key).set(o, ao);
                    }
                }else if(jso.get(key).getClass().equals(jso.getClass())){
                    String clname = o.getClass().getDeclaredField(key).getType().getName();
                    if(clname.trim().indexOf("[") == 0)
                        clname = clname.substring(2, clname.length() - 1);
                    Object no = Class.forName(clname).newInstance();
                    no = json2Object(no, jso.getJSONObject(key), decode);
                    o.getClass().getDeclaredField(key).set(o, no);
                }else{
                    if (o.getClass().getDeclaredField(key) != null) {
                        if(decode)
                            o.getClass().getDeclaredField(key).set(o, URLDecoder.decode((String) jso.get(key).toString(), "UTF-8"));
                        else
                            o.getClass().getDeclaredField(key).set(o, (String) jso.get(key).toString());
                    }
                }
            } catch (Exception e) {
                String emsg = e.getMessage();
            }
        }

        return o;
    }


    // ####################################################################
    // JSONArray to Java Line Object Array
    // ####################################################################
    public Object[] json2ObjectA(Object[] o, JSONArray jsa, boolean decode){
        if(jsa == null)
            return null;
        if(o == null)
            return null;
        if(jsa.length() <= 0)
            return null;
        for(int i = 0; i < jsa.length(); i++)
        {
            try {
                String clname = o.getClass().getName();
                String clnameredux = clname.substring(2, clname.length() - 1);
                o[i] = Class.forName(clnameredux).newInstance();
                if(jsa.get(i).getClass().equals(jsa.getClass())){
                    //is array
                    Object[] ao = json2ObjectA(o, jsa.getJSONArray(i), decode);
                    o[i] = ao;
                }else{
                    // Is object
                    Object oo = json2Object(o[i], jsa.getJSONObject(i), decode);
                    o[i] = oo;
                }
            }catch(Exception e){
                String emsg = e.getMessage();
            }
        }
        return o;
    }

    // ####################################################################
    // Java Class(model) To JSON OBJECT CONVERTER
    // ####################################################################

    public JSONObject mod2json(Object o, boolean encoded) {
        return mod2json(o, encoded, false);
    }
    public JSONObject mod2json(Object o, boolean encoded, boolean ignorenull) {
        if (o == null)
            return null;
        JSONObject jout = new JSONObject();
        for (Field f : o.getClass().getDeclaredFields()) {
            try {
                if (f.get(o) != null && f.get(o).getClass().isArray()) {
                    JSONArray ao = mod2json((Object[]) f.get(o), encoded);
                    jout.put(f.getName(), ao);
                } else {
                    String val = (String) f.get(o);
                    if (val != null)
                        if (encoded)
                            jout.put(f.getName(), URLEncoder.encode((String) f.get(o), "UTF-8"));
                        else
                            jout.put(f.getName(), f.get(o));
                    else
                        if(!ignorenull)
                            jout.put(f.getName(), "");
                }
            } catch (Exception e) {

            }
        }
        return jout;
    }

    // ####################################################################
    // Java Class(model) To JSON ARRAY CONVERTER
    // ####################################################################

    public JSONArray mod2json(Object[] ao, boolean encoded) {
        if (ao == null)
            return null;
        JSONArray jout = new JSONArray();
        for (Object o : ao) {
            JSONObject jso = new JSONObject();
            for (Field f : o.getClass().getDeclaredFields()) {
                try {
                    if (f.get(o) != null && f.get(o).getClass().isArray()) {
                        JSONArray aao = mod2json((Object[]) f.get(o), true);
                        jso.put(f.getName(), aao);
                    } else {
                        String val = (String) f.get(o);
                        if (val != null)
                            if (encoded)
                                jso.put(f.getName(), URLEncoder.encode((String) f.get(o), "UTF-8"));
                            else
                                jso.put(f.getName(), f.get(o));
                        else
                            jso.put(f.getName(), "");
                    }
                } catch (Exception e) {

                }
            }
            jout.put(jso);
        }
        return jout;
    }

    // ####################################################################
    // COMMON LRWS EXCEPTION
    // ####################################################################

    public class LRWSException extends Exception {
        private String e_id = null;
        private String e_msg = null;

        public static final String E_PARAMS = "PAR";
        public static final String E_CONVERSION = "CONV";
        public static final String E_WRAPPER = "WRAP";
        public static final String E_CALLERROR = "CALL";

        public LRWSException(String errid, String emsg) {
            this.e_id = errid;
            this.e_msg = emsg;
        }

        public String getE_id() {
            return e_id;
        }

        public void setE_id(String e_id) {
            this.e_id = e_id;
        }

        public String getE_msg() {
            return e_msg;
        }

        public void setE_msg(String e_msg) {
            this.e_msg = e_msg;
        }


    }
}
