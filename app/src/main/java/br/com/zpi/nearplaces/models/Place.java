package br.com.zpi.nearplaces.models;

/**
 * Created by leonardo on 27/11/16.
 */

public class Place {
    public String id = null;
    public String place_id = null;
    public String name = null;
    public String price_level = null;
    public String rating = null;
    public String formatted_address = null;
    public PlaceGeometry geometry = null;
    public String vicinity = null;
    public String distance = null;
}
