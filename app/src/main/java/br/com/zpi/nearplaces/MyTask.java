package br.com.zpi.nearplaces;

import android.os.AsyncTask;

public abstract class MyTask extends AsyncTask {

    public Object param = null;
    private taskfinished myListener = null;

    public Object getParam() {
        return param;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (myListener != null)
            myListener.onFinishedExec();
        cancel(true);
    }

    public void setParam(String param) {
        this.param = param;
    }

    public void setMyListener(taskfinished myListener) {
        this.myListener = myListener;
    }

    public taskfinished getMyListener() {
        return myListener;
    }

    public interface taskfinished {
        void onFinishedExec();
    }


}
